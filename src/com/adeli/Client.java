package com.adeli;

import java.io.*;
import java.net.*;
import java.util.Scanner;

// Java implementation for a client
// Client class
public class Client {
    public static void main(String[] args) throws IOException {
        try (Scanner scanner = new Scanner(System.in);
             // establish the connection with server port 5056
             Socket socket = new Socket(InetAddress.getByName("localhost"), 5056);
             // obtaining input and out streams
             DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
             DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {


            // the following loop performs the exchange of
            // information between client and client handler
            String request;
            String response;
            while (true) {
                System.out.println(dataInputStream.readUTF());
                request = scanner.nextLine();

                // send request to server
                dataOutputStream.writeUTF(request);

                // If client sends exit,close this connection
                // and then break from the while loop
                if (request.equals("Exit")) {
                    System.out.println("Closing this connection : " + socket);
                    socket.close();
                    System.out.println("Connection closed");
                    break;
                }

                // printing date or time as requested by client
                response = dataInputStream.readUTF();
                System.out.println(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
